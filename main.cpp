#include <iostream>

using std::cout;
using std::cin;
using std::endl;
using std::string;

void toUpperCase(string &s) {
    for (char & i : s)
    {
        i = (char) toupper (i);
    }
}

int main() {
    cout << "Output string via <std::cout> stream" << endl;
    printf("Output string via printf()\n");

    // Define string
    string s1 = "sample string";
    string s2 = "sample string with appended text";

    cout << "s1 length is " << s1.length() << " character(s)" << endl;
    cout << "comparing s1 and s2 " << s1.compare(s2) << endl;
    cout << "finding 't' position in s1 is " << s1.find('t') << endl;

    toUpperCase(s2);

    cout << "s2 in uppercase " << s2 << endl;
    cout << "concatenated strings is '" << s1 + "  " + s2 << "'" << endl;


    return 0;
}
